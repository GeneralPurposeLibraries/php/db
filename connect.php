<?php

include($_SERVER['DOCUMENT_ROOT'].'/db/config.php');

function db_connect()
{
	try {
		return new PDO("mysql:host=".HOST.";dbname=".DATABASE, USER, PASSWORD);
	}
	catch (PDOException $e) {
		die("Error: ".$e->getMessage());
	}
}
